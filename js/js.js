//======================== Variables =============================

const navUl = document.querySelector(".nav__ul");
const ancerLi = document.querySelectorAll(".ancer");
const home = document.querySelector(".bg");
const product = document.querySelector(".product");
const pricing = document.querySelector(".simple");
const tesmonial = document.querySelector(".foto");
const contactUs = document.querySelector(".have");
const buyNow = document.querySelectorAll(".btn");
const btnExit = document.querySelector(".btn--close-modal");
const modal = document.querySelector(".modal");
const overlay = document.querySelector(".overlay");

//======================== Code  NAV CLICK =============================

ancerLi.forEach((e,i)=>{
    e.addEventListener("click",()=>{
        if(i===0){
            console.log("0");
            const s1coords = home.getBoundingClientRect();
            window.scrollTo({
                top: s1coords.top + window.scrollY - 30,
                behavior:"smooth"
            })
        }
        else if(i===1){
            console.log("1");
            const s1coords = product.getBoundingClientRect();
            window.scrollTo({
                top: s1coords.top + window.scrollY - 50,
                behavior:"smooth"
            })
        }
        else if(i===2){
            console.log("1");
            const s1coords = pricing.getBoundingClientRect();
            window.scrollTo({
                top: s1coords.top + window.scrollY,
                behavior:"smooth"
            })
        }
        else if(i===3){
            console.log("1");
            const s1coords = tesmonial.getBoundingClientRect();
            window.scrollTo({
                top: s1coords.top + window.scrollY - 120,
                behavior:"smooth"
            })
        }
        else if(i===4){
            console.log("1");
            const s1coords = contactUs.getBoundingClientRect();
            window.scrollTo({
                top: s1coords.top + window.scrollY - 50,
                behavior:"smooth"
            })
        }
    })
   
})

//======================== Code  BUY NOW CLICK =============================

buyNow.forEach((e,i)=>{
    e.addEventListener("click", ()=>{
        modal.classList.toggle('hidden');
        overlay.classList.toggle('hidden');
    })
})

btnExit.addEventListener("click", ()=>{
    modal.classList.toggle('hidden');
    overlay.classList.toggle('hidden');
})

//======================== Code  Intersection Observer CLICK =============================

const imgTargets = document.querySelectorAll('img[data-src]');
function loadImg(entries, observer) {
  const [entry] = entries;
  if (!entry.isIntersecting) return;
  entry.target.src = entry.target.dataset.src;
  observer.unobserve(entry.target);

  entry.target.addEventListener('load', () => {
    entry.target.classList.remove('lazy-img');
  });
}
const imgObserver = new IntersectionObserver(loadImg, {
  root: null,
  threshold: 1,
  rootMargin: '0px',
});

imgTargets.forEach(img => {
  imgObserver.observe(img);
});


//======================== Code SECTION Intersection Observer CLICK =============================

const allSections = document.querySelectorAll(".section");
function revealSection(entries, observer){
    const [entry] = entries;
    if(!entry.isIntersecting) return ;
    entry.target.classList.remove("section--hidden");
    observer.unobserve(entry.target);
}

let sectionObserver = new IntersectionObserver(revealSection, {
    root:null,
    threshold:0.15,
    rootMargin:"0px"
})

allSections.forEach(section=>{
    sectionObserver.observe(section);
    section.classList.add("section--hidden")
})

//======================== Code SECTION 2 Intersection Observer CLICK =============================
const allSection2 = document.querySelectorAll(".section2");

function revealSection2(entries, observer){
    const [entry] = entries;
    if (!entry.isIntersecting) return ;
    entry.target.classList.remove(".section3--hidden");
    observer.unobserve(entry.target);
}
let imgObserver2 = new IntersectionObserver(revealSection2, {
    root:null,
    threshold:0,
    rootMargin:"0px",
}) 
allSection2.forEach(section2=>{
    imgObserver2.observe(section2);
    section2.classList.add("section3--hidden")
})